# Заметки
~~~
java -cp ./target/classes alx.selivanov.App
~~~
---

# Ссылки
1. [Руководство по Maven](http://www.apache-maven.ru/)
2. [Урок JDBC в примерах](https://habrahabr.ru/sandbox/41444/)
3. [Руководство по JDBC. Пример простого приложения](http://proselyte.net/tutorials/jdbc/simple-application-example/)
4. [JDBC пример](http://java-online.ru/jdbc-example.xhtml)
5. [JDBC API в Java - обзор и туториал](http://www.javenue.info/post/java-jdbc-api)
6. [JDBC - Sample, Example Code](https://www.tutorialspoint.com/jdbc/jdbc-sample-code.htm)
7. [How to add Oracle JDBC driver in your Maven local repository](https://www.mkyong.com/maven/how-to-add-oracle-jdbc-driver-in-your-maven-local-repository/)
8. [Тестирование программы, JUnit](http://java-online.ru/blog-junit.xhtml)
9. [Resolve issues automatically when users push code](https://confluence.atlassian.com/bitbucket/resolve-issues-automatically-when-users-push-code-221451126.html)
10. [Руководство по JDBC. Result Set](http://proselyte.net/tutorials/jdbc/result-set/)
11. [Пять секретов... Соединение Java с базой данных](https://www.ibm.com/developerworks/ru/library/j-5things10/index.html) тут и про пакетные запросы
12. [PG disable/enable triggers](https://www.postgresql.org/docs/9.2/static/sql-altertable.html)
13. [Read/Write CLOB](https://javacubicle.blogspot.ru/2012/07/conversion-from-clob-to-string-in-java.html)   
14. [Чтение и запись LOB объектов](http://java-online.ru/jdbc-lob.xhtml)
15. [Чтение/запись файлов через Java](http://www.sql.ru/faq/faq_topic.aspx?fid=469)
16. [Easiest way to convert a Blob into a byte array](https://stackoverflow.com/questions/6662432/easiest-way-to-convert-a-blob-into-a-byte-array)

  
~~~
для IOUtils требуется appache.commons
    InputStream in = clobObject.getAsciiStream();
    StringWriter w = new StringWriter();
    IOUtils.copy(in, w);
    String clobAsString = w.toString();
~~~

---

# Tasks
1. add connection to oracle
2. add connection to postgre
3. add property files
4. log4j
5. refactoring: oracle -> sourceDB, postgre -> targetDB 
6. 

---

# DB properties
~~~
DB_HOST=oracle01.dote.ru
USER=tisnso_raptor
PASS=tisnso
SID=orcl
CONNECT_STRING=jdbc:oracle:thin:tisnso_raptor/tisnso@oracle01.dote.ru:1521:orcl
~~~
~~~ 
DBMS=postgresql
DB_VER=91
DATABASE=tisnso
DB_HOST=localhost
USER=tisnso_raptor
PASS=tisnso
CONNECT_STRING=jdbc:postgresql://localhost:5432/tisnso?user=tisnso_mixa&password=tisnso?user=tisnso_mixa;password=tisnso
~~~

