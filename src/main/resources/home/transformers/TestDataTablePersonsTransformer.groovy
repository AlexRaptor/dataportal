// TestDataTablePersonsTransformer.groovy

package home.transformers

import alx.selivanov.jdbc.DAOBase
import alx.selivanov.utils.Utils
import org.slf4j.Logger

class TestDataTablePersonsTransformer extends Script {

    @Override
    Object run() {
        final DAOBase targetDAO = args[1]
        final Set<String> tableNames = args[2]
        final List<Map<String, Object>> dataList = args[3]
        final Logger logger = targetDAO.getLogger()

        logger.info ("""modifying data of tables: ${tableNames}""")

        for (Map<String, Object> map : dataList) {
            map.put(Utils.getFirstRightCaseKeyIgnoreCase(map, "comments"), map.get(Utils.getFirstRightCaseKeyIgnoreCase(map, "insuranceNo")) ?: "don't has insuranceNo")
        }

        logger.info ("""records was modified""")
    }

}