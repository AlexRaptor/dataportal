package transformers

import alx.selivanov.jdbc.DAOBase

/**
 *     каждая реализация имеет DAOBase sourceDAO
 *     каждая реализация имеет DAOBase targetDAO
 *     каждая реализация имеет Set<String> tableNames
 *     каждая реализация имеет List<Map<String, Object>> dataList
 *     каждая реализация получает Object extras - объект, возвращённый прошлым типовым трансформером
 */

final DAOBase sourceDAO = args[0]
final DAOBase targetDAO = args[1]
final Set<String> tableNames = args[2]
final List<Map<String, Object>> dataList = args[3]
def extras = args[4]

