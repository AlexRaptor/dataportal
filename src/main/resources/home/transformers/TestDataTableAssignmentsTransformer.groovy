// TestDataTableAssignmentsTransformer.groovy

package home.transformers

import alx.selivanov.utils.Utils

final List<Map<String, Object>> dataList = args[3]

for (Map<String, Object> map : dataList) {
    map.put("ownerId", "persons." + map.get(Utils.getFirstRightCaseKeyIgnoreCase(map, "id")))
}
