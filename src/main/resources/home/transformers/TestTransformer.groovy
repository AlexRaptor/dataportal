// TestTransformer.groovy

package home.transformers

import alx.selivanov.jdbc.DAOBase

final DAOBase sourceDAO = args[0]
final DAOBase targetDAO = args[1]
final Set<String> tableNames = args[2]
final List<Map<String, Object>> dataList = args[3]

println("sourceDAO: " + sourceDAO)
println("targetDAO: " + targetDAO)
println("tableNames: " + tableNames)
println("dataList: " + dataList)