// TestPreImportTransformer.groovy

package home.transformers

import alx.selivanov.jdbc.DAOBase

final DAOBase sourceDAO = args[0]
final DAOBase targetDAO = args[1]
final Set<String> tableNames = args[2]
final List<Map<String, Object>> dataList = args[3]
def extras = args[4]

println("TestPreImportTransformer")
println("EXTRAS: " + extras)
if (extras != null) {
    println("EXTRAS class: " + extras.class)
    extras.first++
    extras.second++
}
println("EXTRAS: " + extras)

return extras ?: ["first": 1, "second" : 2]