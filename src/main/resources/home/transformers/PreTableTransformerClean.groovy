// PreTableTransformerClean.groovy

package home.transformers

import alx.selivanov.jdbc.DAOBase
import org.slf4j.Logger

class PreTableTransformerClean extends Script {

    @Override
    Object run() {
        final DAOBase targetDAO = args[1]
        final Set<String> tableNames = args[2]
        final Logger logger = targetDAO.getLogger()

        logger.info ("""cleaning tables: ${tableNames}""")

        def conn = targetDAO.getConnection()
        conn.setAutoCommit ( false )

        def stmt = conn.createStatement()
        def sql = """DELETE FROM ${tableNames[0]}"""

        def i = stmt.executeUpdate(sql)

        logger.info ("""deleted  ${i} records """)

        conn.setAutoCommit ( true )
        conn.close ( )
    }

}