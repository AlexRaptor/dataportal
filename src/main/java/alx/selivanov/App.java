// App.java

package alx.selivanov;

import alx.selivanov.jdbc.*;
import alx.selivanov.utils.JSONUtils;
import alx.selivanov.utils.SQLUtils;
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Точка входа
 */
public class App {

    private final Set<String> argSet;
    private final Map<String, Object> settingsMap;
    private final Logger rootLogger;
    private final Logger infoLogger;
    private final Logger sourceLogger;
    private final Logger targetLogger;
    private final ConnectionPool sourceCP;
    private final ConnectionPool targetCP;

    private App(String[] args) {
        if (args.length < 1) {
            System.out.println("Send name of file into first launch parameter.");
            System.exit(1);
        }

        String configFileName = args[0];

        System.setProperty("LOGS_PROJECT_DIR", configFileName.substring(0, configFileName.lastIndexOf("/")));
        rootLogger = LoggerFactory.getLogger("rootLogger");
        rootLogger.info("Program started");

        argSet = Arrays.stream(args).map(String::toUpperCase).collect(Collectors.toSet());

        settingsMap = JSONUtils.readJSONfromFileAsMap(configFileName);

        if (settingsMap == null || settingsMap.size() == 0) {
            rootLogger.error("Settings file \"{}\" not found. Send name of file into first launch parameter.", args[0]);
            System.exit(1);
        }

        rootLogger.info("Settings file: {}\n{}", args[0], settingsMap);
        sourceLogger = LoggerFactory.getLogger(DAOSource.class);
        sourceCP = new ConnectionPool((String) ((Map) settingsMap.get("sourceDB")).get("url"), (String) ((Map) settingsMap.get("sourceDB")).get("schema"), sourceLogger);

        if (argSet.contains("INFO") || argSet.contains("SHORTINFO") || argSet.contains("VALIDATE")) {
            infoLogger = LoggerFactory.getLogger("infoLogger");
            targetLogger = null;
            targetCP = null;
        } else if (argSet.contains("DIFF")) {
            infoLogger = LoggerFactory.getLogger("infoLogger");
            targetLogger = LoggerFactory.getLogger(DAOTarget.class);
            targetCP = new ConnectionPool((String) ((Map) settingsMap.get("targetDB")).get("url"), (String) ((Map) settingsMap.get("targetDB")).get("schema"), targetLogger);
        } else {
            infoLogger = null;
            targetLogger = LoggerFactory.getLogger(DAOTarget.class);
            targetCP = new ConnectionPool((String) ((Map) settingsMap.get("targetDB")).get("url"), (String) ((Map) settingsMap.get("targetDB")).get("schema"), targetLogger);
        }
    }

    public static void main(String[] args) {
        App app = new App(args);
        app.run();
    }

    private void run() {
        if (argSet.contains("INFO") || argSet.contains("SHORTINFO")) {
            logSupportsBatchUpdates();
            logInfo(argSet.contains("SHORTINFO"));
        } else if (argSet.contains("DIFF")) {
            logSupportsBatchUpdates();
            logDiff();
        } else {
            startMigration();
        }
    }

    private void logInfo(boolean shortInfo) {
        DAOBase sourceDAO = new DAOOracle(sourceCP);
        String[] mainTablesNameRegExps = (String[]) ((JSONArray) settingsMap.get("mainTablesInclude")).toArray(new String[]{});
        String[] mainExcludedTablesNameRegExps = (String[]) ((JSONArray) settingsMap.get("mainTablesExclude")).toArray(new String[]{});
        String[] secondTablesNameRegExps = (String[]) ((JSONArray) settingsMap.get("secondTablesInclude")).toArray(new String[]{});
        String[] secondExcludedTablesNameRegExps = (String[]) ((JSONArray) settingsMap.get("secondTablesExclude")).toArray(new String[]{});

        infoLogger.info("======= MAIN TABLES =======");
        infoLogger.info(SQLUtils.getDBInfoString(sourceDAO, mainTablesNameRegExps, mainExcludedTablesNameRegExps, shortInfo));

        infoLogger.info("======= SECOND TABLES =======");
        infoLogger.info(SQLUtils.getDBInfoString(sourceDAO, secondTablesNameRegExps, secondExcludedTablesNameRegExps, shortInfo));
    }

    private void logDiff() {
        DAOBase sourceDAO = new DAOOracle(sourceCP);
        DAOBase targetDAO = new DAOPostgre(targetCP);
        String[] mainTablesNameRegExps = (String[]) ((JSONArray) settingsMap.get("mainTablesInclude")).toArray(new String[]{});
        String[] mainExcludedTablesNameRegExps = (String[]) ((JSONArray) settingsMap.get("mainTablesExclude")).toArray(new String[]{});
        String[] secondTablesNameRegExps = (String[]) ((JSONArray) settingsMap.get("secondTablesInclude")).toArray(new String[]{});
        String[] secondExcludedTablesNameRegExps = (String[]) ((JSONArray) settingsMap.get("secondTablesExclude")).toArray(new String[]{});

        infoLogger.info("======= MAIN TABLES =======");
        infoLogger.info(SQLUtils.getDBDiffString(sourceDAO, targetDAO, mainTablesNameRegExps, mainExcludedTablesNameRegExps));

        infoLogger.info("======= SECOND TABLES =======");
        infoLogger.info(SQLUtils.getDBDiffString(sourceDAO, targetDAO, secondTablesNameRegExps, secondExcludedTablesNameRegExps));
    }

    private void logSupportsBatchUpdates() {
        try {
            rootLogger.info("Source DB supportsBatchUpdates = " + sourceCP.getConnection().getMetaData().supportsBatchUpdates());
        } catch (SQLException e) {
            rootLogger.error(e.getMessage(), e);
        }

        try {
            if (targetCP != null) {
                rootLogger.info("Target DB supportsBatchUpdates = " + targetCP.getConnection().getMetaData().supportsBatchUpdates());
            }
        } catch (SQLException e) {
            rootLogger.error(e.getMessage(), e);
        }
    }

    @SuppressWarnings("unchecked")
    private void startMigration() {
        String[] mainTablesNameRegExps = (String[]) ((JSONArray) settingsMap.get("mainTablesInclude")).toArray(new String[]{});
        String[] mainExcludedTablesNameRegExps = (String[]) ((JSONArray) settingsMap.get("mainTablesExclude")).toArray(new String[]{});
        String[] secondTablesNameRegExps = (String[]) ((JSONArray) settingsMap.get("secondTablesInclude")).toArray(new String[]{});
        String[] secondExcludedTablesNameRegExps = (String[]) ((JSONArray) settingsMap.get("secondTablesExclude")).toArray(new String[]{});
        int MAX_THREADS = ((Long) settingsMap.get("threadsCount")).intValue();
        if (MAX_THREADS < 1) {
            MAX_THREADS = 1;
        }

        int MAX_ROWS = ((Long) settingsMap.get("rowOffset")).intValue();
        if (MAX_ROWS < 1) {
            MAX_ROWS = 1;
        }

        Map<String, Object> transformersMap = (Map<String, Object>) settingsMap.get("transformers");
        List<String> preImportTransformers = (List<String>) transformersMap.get("preImportTransformers");
        List<String> postImportTransformers = (List<String>) transformersMap.get("postImportTransformers");

        Map<String, String[]> preTableTransformers = new LinkedHashMap<>();
        Map<String, JSONArray> transformers = (Map<String, JSONArray>) transformersMap.get("preTableTransformers");
        for (Map.Entry<String, JSONArray> entry : transformers.entrySet()) {
            preTableTransformers.put(entry.getKey(), (String[]) entry.getValue().toArray(new String[]{}));
        }

        Map<String, String[]> dataTransformers = new LinkedHashMap<>();
        transformers = (Map<String, JSONArray>) transformersMap.get("dataTransformers");
        for (Map.Entry<String, JSONArray> entry : transformers.entrySet()) {
            dataTransformers.put(entry.getKey(), (String[]) entry.getValue().toArray(new String[]{}));
        }

        Map<String, String[]> postTableTransformers = new LinkedHashMap<>();
        transformers = (Map<String, JSONArray>) transformersMap.get("postTableTransformers");
        for (Map.Entry<String, JSONArray> entry : transformers.entrySet()) {
            postTableTransformers.put(entry.getKey(), (String[]) entry.getValue().toArray(new String[]{}));
        }

        try {
            new DataPortalController(sourceCP, targetCP, mainTablesNameRegExps, mainExcludedTablesNameRegExps, secondTablesNameRegExps, secondExcludedTablesNameRegExps, preImportTransformers, postImportTransformers, preTableTransformers, dataTransformers, postTableTransformers, MAX_THREADS, MAX_ROWS, rootLogger).run();
        } catch (Exception e) {
            rootLogger.error(e.getMessage(), e);
            System.exit(1);
        }
    }

}
