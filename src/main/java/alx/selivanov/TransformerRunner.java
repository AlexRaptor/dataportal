// TransformerRunner.java

package alx.selivanov;

import alx.selivanov.jdbc.ConnectionPool;
import alx.selivanov.jdbc.DAOBase;
import alx.selivanov.jdbc.DAOOracle;
import alx.selivanov.jdbc.DAOPostgre;
import org.apache.bsf.BSFManager;
import org.slf4j.Logger;

import java.io.File;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import static org.apache.commons.io.FileUtils.readFileToString;

public class TransformerRunner implements Callable<Object> {

    private final String filePath;
    private final DAOBase sourceDAO;
    private final DAOBase targetDAO;
    private final Set<String> tableNames;
    private final List<Map<String, Object>> dataList;
    private final Object extras;

    private TransformerRunner(String filePath, DAOBase sourceDAO, DAOBase targetDAO, Set<String> tableNames, List<Map<String, Object>> dataList, Object extras) {
        this.filePath = filePath;
        this.sourceDAO = sourceDAO;
        this.targetDAO = targetDAO;
        this.tableNames = tableNames;
        this.dataList = dataList;
        this.extras = extras;
    }

    public static Object runTransformer(String transformerType, String filePath, ConnectionPool sourceCP, ConnectionPool targetCP, Set<String> tableNames, List<Map<String, Object>> dataList, Logger logger, Object extras) throws Exception {
        DAOBase sourceDAO = new DAOOracle(sourceCP);
        DAOBase targetDAO = new DAOPostgre(targetCP);
        Object result = null;
        Exception lastExc = null;

        TransformerRunner tr = new TransformerRunner(filePath, sourceDAO, targetDAO, tableNames, dataList, extras);
        try {
            logger.info(transformerType + " \"" + filePath + "\" started");
            result = tr.call();
            logger.info(transformerType + " \"" + filePath + "\" finished");
        } catch (Exception e) {
            logger.error(transformerType + " \"" + filePath + "\" ERROR");
            logger.error(e.getMessage(), e);
            lastExc = e;
        }

        try {
            sourceDAO.getConnection().close();
        } catch (SQLException e) {
            sourceDAO.getLogger().error(e.getMessage(), e);
        }
        try {
            targetDAO.getConnection().close();
        } catch (SQLException e) {
            targetDAO.getLogger().error(e.getMessage(), e);
        }

        if (lastExc != null) {
            throw lastExc;
        }

        return result;
    }

    @Override
    public Object call() throws Exception {
        BSFManager.registerScriptingEngine("groovy", "org.codehaus.groovy.bsf.GroovyEngine", new String[] { "groovy" });

        Object[] args = {sourceDAO, targetDAO, tableNames, dataList, extras};
        BSFManager manager = new BSFManager();
        manager.declareBean("args", args, Object[].class);
        return manager.eval("groovy", filePath, 0, 0, readFileToString(new File(filePath), Charset.defaultCharset()));
    }
}
