// Utils.java

package alx.selivanov.utils;

import javax.xml.bind.DatatypeConverter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author raptor
 * date: 07.04.2018.
 */
public class Utils {

    //constants for dates parsing modes
    private static final int DATE_PARSING_MODE_DATE = 0;
    private static final int DATE_PARSING_MODE_TIME = 1;
    private static final int DATE_PARSING_MODE_DATETIME = 2;
    private static final String[] dateFormats = new String[]{"yyyy-MM-dd"};
    private static final String[] timeFormats = new String[]{"HH:mm:ss"};
    private static final String[] dateTimeFormats = new String[]{"yyyy-MM-dd HH:mm:ss"};

    //List of most popular locales that can help in parsing dates
    private static Locale[] POPULAR_LOCALES = new Locale[]
            {
                    Locale.US,
                    new Locale("ru_RU"),
                    Locale.UK,
                    Locale.CANADA,
                    Locale.ENGLISH,
                    Locale.FRANCE,
                    Locale.FRENCH,
                    Locale.GERMAN,
                    Locale.GERMANY,
                    Locale.ITALIAN,
                    Locale.ITALY,
                    Locale.JAPAN,
                    Locale.JAPANESE
            };

    public static <K, V> Map<K, V> filterMapForKeys(Map<K, V> map, final K[] filterKeys) {
        final List<K> filterKeysList = Arrays.asList(filterKeys);
        Map<K, V> resultMap = new LinkedHashMap<>(map.size());
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (filterKeysList.contains(entry.getKey())) {
                resultMap.put(entry.getKey(), entry.getValue());
            }
        }
        return resultMap;
    }

    public static <K, V> List<Map<K, V>> filterListOfMapForKeys(List<Map<K, V>> list, final K[] filterKeys) {
        List<Map<K, V>> resultList = new ArrayList<>(list.size());
        for (Map<K, V> map : list) {
            resultList.add(filterMapForKeys(map, filterKeys));
        }
        return resultList;
    }

    public static String beautifyListOfMaps(List<Map<String, String>> list) {
        StringBuilder str = new StringBuilder();

        if (list != null) {
            for (Map map : list) {
                str.append(map + "\n");
            }
            str.deleteCharAt(str.length() - 1);
        }
        return str.toString();
    }

    public static boolean containsStringIgnoreCase(Collection<String> list, String string) {
        if (string == null) {
            return false;
        }

        for (String item : list) {
            if (string.equalsIgnoreCase(item)) {
                return true;
            }
        }

        return false;
    }

    public static String getFirstRightCaseKeyIgnoreCase(Map<String, ?> map, String key) {
        if (key == null) {
            return null;
        }

        for (String mapKey : map.keySet()) {

            if (mapKey.equalsIgnoreCase(key)) {
                return mapKey;
            }
        }

        return null;
    }

    public static String getStringIfContainsIgnoreCase(Collection<String> list, String string) {
        if (string == null) {
            return null;
        }

        for (String item : list) {
            if (string.equalsIgnoreCase(item)) {
                return item;
            }
        }

        return null;
    }

    public static <T> T changeType(Object val, Class<T> valClass) throws ParseException, DatatypeConfigurationException {
        return (T) changeType(val, valClass, Locale.getDefault());
    }

    /**
     * This method changes type of the passed value to valClass type. Value to convert can be array,
     * then each element of that array will be convert with this method.
     * <br/>This change includes parsing strings into folowing types:
     * <@link java.lang.Double>
     * <br/>{@link java.lang.Float}
     * <br/>{@link java.lang.Byte}
     * <br/>{@link java.lang.Short}
     * <br/>{@link java.lang.Integer}
     * <br/>{@link java.lang.Boolean}
     * <br/>{@link java.math.BigDecimal}
     * <br/>{@link java.math.BigInteger}
     * <br/>{@link java.sql.Date}
     * <br/>{@link java.util.Date}
     * <br/>{@link java.sql.Time}
     * <br/>{@link java.sql.Timestamp}
     * <br/>{@link java.io.File}
     * <p>
     * <br/><br/>Also this method can convert from <@link java.util.Date> to {@link java.sql.Date}.
     *
     * @param val      src value
     * @param valClass resulting value type
     * @param locale   user locale
     */
    private static Object changeType(Object val, Class valClass, Locale locale) throws ParseException, DatatypeConfigurationException {
        if (val == null || valClass == null) {
            return null;
        }

        if (val.getClass().equals(valClass)) {
            return val;
        }

        if (val.getClass().isArray() && valClass.isArray() &&
                !val.getClass().getComponentType().equals(valClass.getComponentType())) {
            Object[] vals = (Object[]) val;
            if (vals.length == 0) {
                return null;
            }
            ArrayList out = new ArrayList();
            for (int i = 0; i < vals.length; i++) {
                Object newVal = changeType(vals[i], valClass.getComponentType(), locale);
                //when submitted String array cannot be converted to Timestamp array, it may mean
                //that although the column type is Timestamp, it should be handled as java.sql.Date type
                //we just return the _val_ unchanged, it will be checked in addRecordFilter() and there will
                // be an attempt to parse it as Date
                if (java.sql.Timestamp.class.equals(valClass.getComponentType()) && newVal.equals(vals[i])) {
                    return val;
                }

                out.add(newVal);
            }
            return out.toArray((Object[]) java.lang.reflect.Array.newInstance(valClass.getComponentType(), 0));
        }

        if (java.sql.Date.class.equals(valClass) &&
                val instanceof java.util.Date) {
            return new java.sql.Date(((java.util.Date) val).getTime());
        }

        if (java.sql.Date.class.equals(valClass) &&
                val instanceof java.util.Calendar) {
            return new java.sql.Date(((java.util.Calendar) val).getTime().getTime());
        }

        if (java.util.Date.class.equals(valClass) &&
                val instanceof java.sql.Date) {
            return new java.util.Date(((java.sql.Date) val).getTime());
        }

        if (java.util.Date.class.equals(valClass) &&
                val instanceof XMLGregorianCalendar) {
            XMLGregorianCalendar xcal = (XMLGregorianCalendar) val;
            return xcal.toGregorianCalendar().getTime();
        }

        if (java.sql.Date.class.equals(valClass) &&
                val instanceof XMLGregorianCalendar) {
            XMLGregorianCalendar xcal = (XMLGregorianCalendar) val;
            return new java.sql.Date(xcal.toGregorianCalendar().getTime().getTime());
        }

        if (javax.xml.datatype.XMLGregorianCalendar.class.equals(valClass) &&
                val instanceof java.util.Date) {
            try {
                GregorianCalendar gc = new GregorianCalendar();
                gc.setTime((java.util.Date) val);
                return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
            } catch (DatatypeConfigurationException ignore) {
                return val;
            }
        }

        if (javax.xml.datatype.XMLGregorianCalendar.class.equals(valClass) &&
                val instanceof String) {
            try {
                return DatatypeConverter.parseDateTime((String) val);
            } catch (IllegalArgumentException ignore) {
                return val;
            }
        }


        if (java.util.Date.class.equals(valClass) &&
                val instanceof java.sql.Timestamp) {
            return new java.util.Date(((java.sql.Timestamp) val).getTime());
        }

        if (String.class.equals(valClass) && "org.mozilla.javascript.NativeString".equals(val.getClass().getName())) {
            return val.toString();
        }

        if (val instanceof Boolean && String.class.equals(valClass)) {
            return val.toString();
        }

        if (val instanceof Number) {
            if (String.class.equals(valClass)) {
                return val.toString();
            }
            if (int.class.equals(valClass) || Integer.class.equals(valClass)) {
                return Integer.valueOf(((Number) val).intValue());
            }
            if (long.class.equals(valClass) || Long.class.equals(valClass)) {
                return Long.valueOf(((Number) val).longValue());
            }
            if (BigDecimal.class.equals(valClass)) {
                return new BigDecimal(((Number) val).doubleValue());
            }
        }

        if ("org.mozilla.javascript.NativeString".equals(val.getClass().getName())) {
            val = val.toString();
        }

        if (!(val instanceof String)) {
            return val;
        }

        if ("".equals(val)) {
            return null;
        }

        if (Double.class.equals(valClass) || double.class.equals(valClass)) {
            return Double.valueOf(fixNumber((String) val, false));
        }
        if (Float.class.equals(valClass) || float.class.equals(valClass)) {
            return Float.valueOf(fixNumber((String) val, false));
        }
        if (Byte.class.equals(valClass) || byte.class.equals(valClass)) {
            return Byte.valueOf(fixNumber((String) val, true));
        }
        if (Short.class.equals(valClass) || short.class.equals(valClass)) {
            return Short.valueOf(fixNumber((String) val, true));
        }
        if (Integer.class.equals(valClass) || int.class.equals(valClass)) {
            return Integer.valueOf(fixNumber((String) val, true));
        }
        if (Long.class.equals(valClass) || long.class.equals(valClass)) {
            return Long.valueOf(fixNumber((String) val, true));
        }
        if (Boolean.class.equals(valClass) || boolean.class.equals(valClass)) {
            String s = ((String) val).toLowerCase();
            return Boolean.valueOf("true".equals(s) || "on".equals(s) ||
                    "yes".equals(s) || "1".equals(s));
        }

        if (BigDecimal.class.equals(valClass)) {
            return new BigDecimal(fixNumber((String) val, false));
        }
        if (BigInteger.class.equals(valClass)) {
            return new BigInteger(fixNumber((String) val, false));
        }

        if (java.util.Date.class.equals(valClass)) {
            DateFormat df = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
            df.setLenient(false);
            java.util.Date parsed = null;
            try {
                parsed = df.parse((String) val);
            } catch (ParseException pe) {
                parsed = parseDateWithOtherLocales((String) val, DATE_PARSING_MODE_DATE);
            }
            return parsed;
        }
        if (java.sql.Date.class.equals(valClass)) {
            DateFormat df = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
            df.setLenient(false);
            java.util.Date parsed = null;
            try {
                parsed = df.parse((String) val);
            } catch (ParseException pe) {
                parsed = parseDateWithOtherLocales((String) val, DATE_PARSING_MODE_DATE);
            }

            return new java.sql.Date(parsed.getTime());
        }
        if (java.sql.Time.class.equals(valClass)) {
            DateFormat df = DateFormat.getTimeInstance(DateFormat.DEFAULT, locale);
            df.setLenient(false);
            java.util.Date parsed = null;
            try {
                parsed = df.parse((String) val);
            } catch (ParseException pe) {
                parsed = parseDateWithOtherLocales((String) val, DATE_PARSING_MODE_TIME);
            }

            return new java.sql.Time(parsed.getTime());
        }
        if (java.sql.Timestamp.class.equals(valClass)) {
            String str = (String) val;

            boolean isHtml5 = str.length() == 16 && str.charAt(10) == 'T';
            if (isHtml5) {
                val = Utils.subst(str, "T", " ") + ":00";
            }

            java.util.Date parsed = null;
            DateFormat df = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, locale);
            df.setLenient(false);
            try {
                parsed = df.parse((String) val);
            } catch (ParseException pe) {
                try {
                    parsed = parseDateWithOtherLocales((String) val, DATE_PARSING_MODE_DATETIME);
                } catch (ParseException pe2) {
                    df = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
                    try {
                        parsed = df.parse((String) val);
                    } catch (ParseException pe3) {
                        parsed = parseDateWithOtherLocales((String) val, DATE_PARSING_MODE_DATE);
                    }
                }
            }

            return new java.sql.Timestamp(parsed.getTime());
        }
        if (javax.xml.datatype.XMLGregorianCalendar.class.equals(valClass)) {
            GregorianCalendar gc = new GregorianCalendar();
            DateFormat df = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
            df.setLenient(false);
            java.util.Date parsed = null;
            try {
                parsed = df.parse((String) val);
            } catch (ParseException pe) {
                parsed = parseDateWithOtherLocales((String) val, DATE_PARSING_MODE_DATE);
            }
            gc.setTime(parsed);
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
        }

        if (File.class.equals(valClass)) {
            return new File((String) val);
        }

        return val;
    }

    private static String fixNumber(String number, boolean isInt) {
        if (number == null) {
            return null;
        }
        number = subst(number, " ", "");
        int pointInd = number.lastIndexOf('.');
        int commaInd = number.lastIndexOf(',');

        if (commaInd > 0 && pointInd > commaInd) {
            return subst(number, ",", "");
        }
        if (!isInt && commaInd > 0) {
            return subst(number, ",", ".");
        }

        if (isInt) {
            try {
                Long.valueOf(number);
            } catch (NumberFormatException nfe) {
                if (number.endsWith(".0")) {
                    return subst(number, ".0", "");
                }
            }
        }

        return number;
    }

    /**
     * Substitute string "fromText" in "text" for string "toText".
     * Substituted text will be returned as a result.
     *
     * @param text     text, where fromText is substituting for another text.
     * @param fromText text for substituting
     * @param toText   text, that is substituting fromText
     * @return returns substituted text
     */
    private static String subst(String text, String fromText, String toText) {
        return subst(text, fromText, toText, "");
    }

    /**
     * Substitute string "fromText" in "text" for another string.
     * Substitution string is "toText" or, if "toText" is empty (isEmpty), then "defText".
     * Substituted text will be returned as a result.
     *
     * @param text     text, where fromText is substituting for another text.
     * @param fromText text for substituting
     * @param toText   text, that is substituting fromText
     * @param defText  text, that is substituting fromText, if "toText" is empty (isEmpty)
     * @return returns substituted text
     */
    private static String subst(String text, String fromText, String toText, String defText) {
        if (text == null) {
            return text;
        }
        int prevPos = 0;
        String newText = toText == null || "".equals(toText) ? defText : toText;
        for (int pos = text.indexOf(fromText, prevPos); pos >= 0;
             pos = text.indexOf(fromText, prevPos + newText.length())) {
            prevPos = pos;
            text = new StringBuffer(text).replace(pos, pos + fromText.length(), newText).toString();
        }
        return text;
    }

    /**
     * Parse date using popular locales.
     * When failed, throws ParseException.
     * It is needed to try all locales when the date could not
     * be parsed with user's locale.
     *
     * @param val
     * @return parsed date
     * @throws ParseException when none of the locales helps.
     */
    private static Date parseDateWithOtherLocales(String val, int parsingMode) throws ParseException {
        ParseException lastException = null;
        for (int i = 0; i < POPULAR_LOCALES.length; i++) {
            try {
                DateFormat df = null;
                //dates and times are handled differently
                switch (parsingMode) {
                    case DATE_PARSING_MODE_DATE:
                        df = DateFormat.getDateInstance(DateFormat.DEFAULT, POPULAR_LOCALES[i]);
                        break;
                    case DATE_PARSING_MODE_TIME:
                        df = DateFormat.getTimeInstance(DateFormat.DEFAULT, POPULAR_LOCALES[i]);
                        break;
                    case DATE_PARSING_MODE_DATETIME:
                        df = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, POPULAR_LOCALES[i]);
                        break;
                    default:
                        df = DateFormat.getDateInstance(DateFormat.DEFAULT, POPULAR_LOCALES[i]);
                }
                //return when parsed successfully
                df.setLenient(false);
                java.util.Date parsed = df.parse(val);
                return parsed;
            } catch (ParseException pe) {
                //could not parse, continue with other locales.
                lastException = pe;
            }
        }

        //now try other specific date/time patterns
        String[] formats = null;
        switch (parsingMode) {
            case DATE_PARSING_MODE_DATE:
                formats = dateFormats;
                break;
            case DATE_PARSING_MODE_TIME:
                formats = timeFormats;
                break;
            case DATE_PARSING_MODE_DATETIME:
                formats = dateTimeFormats;
                break;
            default:
                formats = dateFormats;
        }

        for (int i = 0; i < formats.length; i++) {
            String pattern = formats[i];
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            sdf.setLenient(false);
            try {
                return sdf.parse(val);
            } catch (ParseException pe) {
                //could not parse, continue with other patterns.
                lastException = pe;
            }
        }

        //the string was not parsed, throw an exception.
        throw lastException;
    }

    private Utils() {}
}
