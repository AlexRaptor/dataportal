// SQLUtils.java

package alx.selivanov.utils;

import alx.selivanov.DataPortalConstants;
import alx.selivanov.jdbc.DAOBase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static alx.selivanov.utils.Utils.beautifyListOfMaps;

public class SQLUtils {

    public static String getDBInfoString(DAOBase dao, String[] tableNamesExpressions, String[] excludedTableNameRegExps, boolean shortInfo) {
        List<String> tableNames = dao.getTableNames(tableNamesExpressions, excludedTableNameRegExps);
        StringBuilder str = new StringBuilder();

        for (String table : tableNames) {
            Map<String, Object> tableDescription = dao.getTableDescribe(table);
            str.append("\nTABLE: " + table);
            str.append("\n===================================================\n");
            str.append("SIZE (MB) = " + tableDescription.get("SIZE") + ", ROW COUNT = " + tableDescription.get("ROW_COUNT"));
            str.append("\n---------------------------------------------------\n");

            if (!shortInfo) {
                str.append("COLUMNS:\n");
                str.append(beautifyListOfMaps((List<Map<String, String>>) tableDescription.get("COLUMNS")));
                str.append("\n---------------------------------------------------\n");
                str.append("PRIMARY KEYS:\n");
                str.append(tableDescription.get("PK"));
                str.append("\n---------------------------------------------------\n");
                str.append("FOREIGN KEYS:\n");
                str.append(tableDescription.get("FK"));
            }
        }

        return str.toString();
    }

    public static String getDBDiffString(DAOBase sourceDAO, DAOBase targetDAO, String[] tableNamesExpressions, String[] excludedTableNameRegExps) {
        List<String> sourceTableNames = sourceDAO.getTableNames(tableNamesExpressions, excludedTableNameRegExps);
        List<String> targetTableNames = targetDAO.getTableNames(tableNamesExpressions, excludedTableNameRegExps);
        StringBuilder str = new StringBuilder();

        for (String tableName : sourceTableNames) {
            String targetTableName = Utils.getStringIfContainsIgnoreCase(targetTableNames, tableName);
            if (targetTableName == null) {
                Map<String, Object> sourceTableDescription = sourceDAO.getTableDescribe(tableName);
                str.append("\nTABLE: \"" + tableName + "\" MISSED in target DB !!!");
                str.append("\n===================================================\n");
                str.append("SIZE (MB) = " + sourceTableDescription.get("SIZE") + ", ROW COUNT = " + sourceTableDescription.get("ROW_COUNT"));
                str.append("\n---------------------------------------------------\n");
                str.append("COLUMNS:\n");
                str.append(beautifyListOfMaps((List<Map<String, String>>) sourceTableDescription.get("COLUMNS")));
                str.append("\n---------------------------------------------------\n");
                str.append("PRIMARY KEYS:\n");
                str.append(sourceTableDescription.get("PK"));
                str.append("\n---------------------------------------------------\n");
                str.append("FOREIGN KEYS:\n");
                str.append(sourceTableDescription.get("FK"));
            } else {
                Map<String, Object> sourceTableDescription = sourceDAO.getTableDescribe(tableName);
                List<Map<String, String>> sourceTableColumns = (List<Map<String, String>>) sourceTableDescription.get("COLUMNS");
                List<Map<String, String>> targetTableColumns = targetDAO.getTableColumns(targetTableName, new String[]{"COLUMN_NAME"});
                List<String> targetTableColumnNames = new ArrayList<>(targetTableColumns.size());

                for (Map<String, String> columnMap : targetTableColumns) {
                    targetTableColumnNames.add(columnMap.get("COLUMN_NAME"));
                }

                List<Map<String, String>> diffColumns = new ArrayList<>(sourceTableColumns);

                for (Map<String, String> map : sourceTableColumns) {
                    if (Utils.containsStringIgnoreCase(targetTableColumnNames, map.get("COLUMN_NAME"))) {
                        diffColumns.remove(map);
                    }
                }

                if (diffColumns.size() > 0) {
                    str.append("\nTABLE: " + tableName);
                    str.append("\n===================================================\n");
                    str.append("\n---------------------------------------------------\n");
                    str.append("COLUMNS, MISSED in target DB !!!:\n");
                    str.append(beautifyListOfMaps(diffColumns));
                }
            }
        }

        return str.toString();
    }

    public static String getExcludedColumnName(String columnName) {
        return DataPortalConstants.EXCUDED_COLUMN_PREFIX + columnName;
    }

    private SQLUtils() {}
}
