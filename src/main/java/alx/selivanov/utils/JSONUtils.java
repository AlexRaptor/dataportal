// JSONUtils.java

package alx.selivanov.utils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class JSONUtils {

    static final Logger rootLogger = LoggerFactory.getLogger("rootLogger");

    public static Map<String, Object> readJSONfromFileAsMap(String filePath) {
        JSONParser parser = new JSONParser();

        try {
            Object obj = parser.parse(new FileReader(filePath));
            JSONObject jsonObject = (JSONObject) obj;
            Map<String, Object> map = new HashMap<>(jsonObject.size());

            for (Object key : jsonObject.keySet()) {
                Object currentObject = jsonObject.get(key);
                map.put(key.toString(), currentObject);
            }

            return map;

        } catch (Exception e) {
            rootLogger.error(e.getMessage(), e);
        }

        return null;
    }

}
