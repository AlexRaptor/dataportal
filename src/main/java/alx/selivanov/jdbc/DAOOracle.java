// DAOOracle.java

package alx.selivanov.jdbc;

import alx.selivanov.utils.SQLUtils;

import java.util.Map;

public class DAOOracle extends DAOBase implements DAOSource {

    public DAOOracle(ConnectionPool connectionPool) {
        super(connectionPool);
    }

    @Override
    public Double getTableSize(String tableName) {
        String sql = "SELECT sum(bytes) / 1024 / 1024 AS \"SIZE\"\n" +
                "FROM\n" +
                "  (SELECT bytes\n" +
                "   FROM dba_segments\n" +
                "   WHERE owner = '" + getSchemaName() + "'\n" +
                "         AND segment_type IN ('TABLE', 'TABLE PARTITION', 'TABLE SUBPARTITION')\n" +
                "         AND segment_name = '" + tableName + "'\n" +
                "   UNION ALL\n" +
                "   SELECT s.bytes\n" +
                "   FROM dba_lobs l, dba_segments s\n" +
                "   WHERE l.owner = '" + getSchemaName() + "'\n" +
                "         AND s.segment_name = l.segment_name\n" +
                "         AND s.owner = l.owner\n" +
                "         AND s.segment_type IN ('LOBSEGMENT', 'LOB PARTITION')\n" +
                "         AND l.table_name = '" + tableName + "'\n" +
                "    UNION\n" +
                "    SELECT 0 FROM DUAL\n" +
                "  )";

        Map map = readAsMap(sql);
        String sizeStr = (String) map.get("SIZE");

        return sizeStr != null ? Double.valueOf(sizeStr) : 0;
    }

    @Override
    public String getSelectAllFromRowToRowQuery(String tableName, long beginRowNum, long endRowNum) {
        return "SELECT x.*\n" +
                "FROM (SELECT ROWNUM AS \"" + SQLUtils.getExcludedColumnName("RowNumber") + "\", t.*\n" +
                "      FROM " + tableName + " t) x\n" +
                "WHERE x.\"" + SQLUtils.getExcludedColumnName("RowNumber") + "\" BETWEEN " + beginRowNum + " AND " + endRowNum;
    }

}
