// DAOPostgre.java

package alx.selivanov.jdbc;

import alx.selivanov.utils.SQLUtils;

import java.util.Map;

public class DAOPostgre extends DAOBase implements DAOTarget {

    public DAOPostgre(ConnectionPool connectionPool) {
        super(connectionPool);
    }

    @Override
    public Double getTableSize(String tableName) {
        String sql = "SELECT CAST(PG_TABLE_SIZE('" + tableName + "') AS FLOAT ) / 1024 / 1024 AS \"SIZE\"";

        Map map = readAsMap(sql);
        String sizeStr = (String) map.get("SIZE");

        return sizeStr != null ? Double.valueOf(sizeStr) : 0;
    }


    @Override
    public String getSelectAllFromRowToRowQuery(String tableName, long beginRowNum, long endRowNum) {
        return "SELECT x.*\n" +
                "FROM (SELECT ROW_NUMBER() OVER () AS \"" + SQLUtils.getExcludedColumnName("RowNumber") + "\", t.*\n" +
                "      FROM " + tableName + " t) x\n" +
                "WHERE x.\"" + SQLUtils.getExcludedColumnName("RowNumber") + "\" BETWEEN " + beginRowNum + " AND " + endRowNum;
    }

}
