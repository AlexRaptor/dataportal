// DAOBase.java

package alx.selivanov.jdbc;

import alx.selivanov.utils.Utils;
import org.slf4j.Logger;

import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public abstract class DAOBase {

    private final ConnectionPool connectionPool;
    private Connection connection;
    private final String schemaName;
    private final Logger logger;

    public abstract Double getTableSize(String tableName);
    public abstract String getSelectAllFromRowToRowQuery(String tableName, long beginRowNum, long endRowNum);

    DAOBase(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
        this.schemaName = getRightCaseString(connectionPool.getSchemaName());
        this.logger = connectionPool.getLogger();
    }

    public Long getTableRowCount(String tableName) {
        String sql = "SELECT COUNT(1) AS \"COUNT\" FROM " + tableName;

        Map map = readAsMap(sql);
        String countStr = (String) map.get("COUNT");

        return countStr != null ? Long.valueOf(countStr) : 0;
    }

    private DatabaseMetaData getDatabaseMetadata() {
        try {
            return getConnection().getMetaData();
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }

        return null;
    }

    private String getRightCaseString(String string) {
        DatabaseMetaData databaseMetaData = getDatabaseMetadata();

        try {
            if (databaseMetaData != null) {
                if (databaseMetaData.storesLowerCaseIdentifiers()) {
                    return string.toLowerCase();
                } else if (databaseMetaData.storesUpperCaseIdentifiers()) {
                    return string.toUpperCase();
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }

        return string;
    }

    public List<Map<String, String>> getTables() {
        DatabaseMetaData databaseMetaData = getDatabaseMetadata();
        List<Map<String, String>> result = new ArrayList<>();

        if (databaseMetaData != null) {
            try (ResultSet tables = databaseMetaData.getTables(null, schemaName, "%", new String[]{"TABLE"})) {
                result = QueryExecuter.resultSetToListOfStringMaps(getConnection(), tables);
            } catch (SQLException | IOException | ClassNotFoundException e) {
                logger.error(e.getMessage(), e);
            }
        }

        return result;
    }

    public List<String> getTableNames(String[] tableNameRegExps, String[] excludedTableNameRegExps) {
        DatabaseMetaData databaseMetaData = getDatabaseMetadata();
        List<String> allTableNames = new ArrayList<>();

        if (databaseMetaData != null) {
            try (ResultSet tablesRS = databaseMetaData.getTables(null, schemaName, "%", new String[]{"TABLE"})) {
                while (tablesRS.next()) {
                    allTableNames.add(tablesRS.getString("TABLE_NAME"));
                }
            } catch (SQLException | NullPointerException e) {
                logger.error(e.getMessage(), e);
            }
        }

        List<String> result = new ArrayList<>();
        for (String tableName : tableNameRegExps) {
            Pattern pattern = Pattern.compile(tableName);
            result.addAll(allTableNames.stream().filter(pattern.asPredicate()).collect(Collectors.toList()));
        }

        for (String tableName : excludedTableNameRegExps) {
            Pattern pattern = Pattern.compile(tableName);
            result.removeAll(allTableNames.stream().filter(pattern.asPredicate()).collect(Collectors.toList()));
        }

        return result;
    }

    public Map<String, Object> getTableDescribe(String tableName) {
        Map<String, Object> result = new LinkedHashMap<>(4);

        try {
            result.put("SIZE", getTableSize(tableName).toString());
            result.put("ROW_COUNT", getTableRowCount(tableName).toString());
            result.put("COLUMNS", getTableColumns(tableName,
                    new String[]{"COLUMN_NAME", "TYPE_NAME", "COLUMN_SIZE", "IS_NULLABLE", "DECIMAL_DIGITS", "NUM_PREC_RADIX", "IS_NULLABLE"}));
            result.put("PK", getPrimaryKeys(tableName));
            result.put("FK", getForeignKeys(tableName));
        } catch (Exception e) {
            logger.error(e.getMessage() + "\nTABLE: " + schemaName + "." + tableName, e);
        }

        return result;
    }

    public List<Map<String, String>> getTableColumns(String tableName) {
        return getTableColumns(tableName, null);
    }

    public List<Map<String, String>> getTableColumns(String tableName, String[] filterKeys) {
        DatabaseMetaData databaseMetaData = getDatabaseMetadata();
        List<Map<String, String>> result = new ArrayList<>();

        try (ResultSet columnsRS = Objects.requireNonNull(databaseMetaData).getColumns(null, schemaName, tableName, null)) {
            result = QueryExecuter.resultSetToListOfStringMaps(getConnection(), columnsRS);
        } catch (SQLException | IOException | ClassNotFoundException e) {
            logger.error(e.getMessage(), e);
        }
        if (filterKeys != null && filterKeys.length > 0) {
            result = Utils.filterListOfMapForKeys(result, filterKeys);
        }
        return result;
    }

    public Map<String, Class> getTableColumnClasses(String tableName) {
        Map<String, Class> rowData = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        String sql = "SELECT * FROM " + tableName + " WHERE 1 != 0";

        logger.debug(sql);

        try (Statement statement = getConnection().createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

            for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
                Class columnClass = Class.forName(resultSetMetaData.getColumnClassName(i));
                rowData.put(resultSetMetaData.getColumnLabel(i), columnClass);
            }
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e.getMessage(), e);
        }

        return rowData;
    }

    private Map<String, String> getPrimaryKeys(String tableName) {
        DatabaseMetaData databaseMetaData = getDatabaseMetadata();
        Map<String, String> primaryKeysMap = new LinkedHashMap<>();

        try (ResultSet PK = Objects.requireNonNull(databaseMetaData).getPrimaryKeys(null, schemaName, tableName)) {
            while (PK.next()) {
                primaryKeysMap.put(PK.getString("COLUMN_NAME"), PK.getString("PK_NAME"));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }

        return primaryKeysMap;
    }

    private Map<String, String> getForeignKeys(String tableName) {
        DatabaseMetaData databaseMetaData = getDatabaseMetadata();
        Map<String, String> foreignKeysMap = new LinkedHashMap<>();

        try (ResultSet FK = Objects.requireNonNull(databaseMetaData).getImportedKeys(null, schemaName, tableName)) {
            while (FK.next()) {
                foreignKeysMap.put(FK.getString("PKTABLE_NAME") + "." + FK.getString("PKCOLUMN_NAME"), FK.getString("FKTABLE_NAME") + "." + FK.getString("FKCOLUMN_NAME"));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }

        return foreignKeysMap;
    }

    /**
     * Returns data from first row
     * @param sql SQL-query
     * @return key -> value from first row
     */
    public Map readAsMap(String sql) {
        return readAsMap(getConnection(), logger, sql);
    }

    /**
     * Returns data from first row
     * @param connection Connection
     * @param logger Logger
     * @param sql SQL-query
     * @return key -> value from first row
     */
    public static Map readAsMap(Connection connection, Logger logger, String sql) {
        final Map[] result = new Map[]{new LinkedHashMap<>()};

        try {
            new QueryExecuter(connection, logger) {
                @Override
                public void use(Map<String, Object> rowData, int rowNumber) {
                    result[0] = rowData;
                }
            }.executeQuery(sql, true);
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }

        return result[0];
    }

    public List<Map<String, Object>> readAsListOfMaps(String sql) {
        return readAsListOfMaps(getConnection(), logger, sql);
    }

    public static List<Map<String, Object>> readAsListOfMaps(Connection connection, Logger logger, String sql) {
        List<Map<String, Object>> result = new ArrayList<>();

        try {
            new QueryExecuter(connection, logger) {
                @Override
                public void use(Map<String, Object> rowData, int rowNumber) {
                    result.add(rowNumber - 1, rowData);
                }
            }.executeQuery(sql);
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }

        return result;
    }

    public List<List<Object>> readAsListOfLists(String sql) {
        return readAsListOfLists(getConnection(), logger, sql);
    }

    public static List<List<Object>> readAsListOfLists(Connection connection, Logger logger, String sql) {
        List<List<Object>> result = new ArrayList<>();

        try {
            new QueryExecuter(connection, logger) {
                @Override
                public void use(Map<String, Object> rowData, int rowNumber) {
                    result.add(rowNumber - 1, new ArrayList<>(rowData.values()));
                }
            }.executeQuery(sql);
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }

        return result;
    }

    public ConnectionPool getConnectionPool() {
        return connectionPool;
    }

    public Connection getConnection() {
        try {
            if (connection == null || connection.isClosed())
            {
                this.connection = connectionPool.getConnection();
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        }

        return connection;
    }
    public Logger getLogger() {
        return logger;
    }

    public String getSchemaName() {
        return schemaName;
    }

    @Override
    protected void finalize() throws Throwable {
        if (connection != null && !connection.isClosed()) {
            connection.close();
        }
    }
}
