// ConnectionPool.java

package alx.selivanov.jdbc;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPool {

    private final Logger logger;
    private HikariDataSource ds;
    private final String url;
    private final String schemaName;

    public ConnectionPool(String url, String schemaName, Logger logger) {
        this.logger = logger;
        try {
            HikariConfig config = new HikariConfig();
            config.setJdbcUrl(url);
            config.setMaximumPoolSize(100);
            config.setMinimumIdle(20);
            config.setMaxLifetime(30000);
            ds = new HikariDataSource(config);
            logger.debug("connection '{}' established", url);
        } catch (Exception e) {
            logger.error(e.getMessage() + "\nurl: " + url, e);
        }
        this.url = url;
        this.schemaName = schemaName;
    }

    public Connection getConnection() {
        try {
            return ds.getConnection();
        } catch (SQLException e) {
            logger.error(e.getMessage() + "\nurl: " + url, e);
        }
        return null;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public Logger getLogger() {
        return logger;
    }

}