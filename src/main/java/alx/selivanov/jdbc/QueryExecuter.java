// QueryExecuter.java

package alx.selivanov.jdbc;

import alx.selivanov.DataPortalConstants;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class QueryExecuter {

    private final Logger logger;
    private Connection connection;

    public abstract void use(Map<String, Object> rowData, int rowNumber);
    
    QueryExecuter(Connection connection, Logger logger) {
        this.logger = logger;
        this.connection = connection;
    }

    private Map<String, Object> resultSetToObjectMap(ResultSet resultSet) throws SQLException, ClassNotFoundException, IOException {
        return resultSetToObjectMap(connection, resultSet);
    }

    private static Map<String, Object> resultSetToObjectMap(Connection connection, ResultSet resultSet) throws SQLException, ClassNotFoundException, IOException {
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        Map<String, Object> rowData = new LinkedHashMap<>(resultSetMetaData.getColumnCount());

        for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
            String columnLabel = resultSetMetaData.getColumnLabel(i);
            if (!columnLabel.trim().toLowerCase().startsWith(DataPortalConstants.EXCUDED_COLUMN_PREFIX)) {
                Object obj = resultSet.getObject(i);

                Class columnClass = Class.forName(resultSetMetaData.getColumnClassName(i));

                if (Clob.class.isAssignableFrom(columnClass)) {
                    obj = clobToString((Clob) obj);
                } else if (Blob.class.isAssignableFrom(columnClass)) {
                    obj = blobToByteArray((Blob) obj);
                } else if (obj != null){
                    obj = obj.toString();
                }

                rowData.put(columnLabel, obj);
            }
        }

        return rowData;
    }

    private List<Map<String, Object>> resultSetToListOfObjectMaps(ResultSet resultSet) throws SQLException, ClassNotFoundException, IOException {
        return resultSetToListOfObjectMaps(connection, resultSet);
    }

    private static List<Map<String, Object>> resultSetToListOfObjectMaps(Connection connection, ResultSet resultSet) throws SQLException, ClassNotFoundException, IOException {
        List<Map<String, Object>> result = new ArrayList<>();

        while (resultSet.next()) {
            result.add(resultSetToObjectMap(connection, resultSet));
        }

        return result;
    }

    static List<Map<String, String>> resultSetToListOfStringMaps(Connection connection, ResultSet resultSet) throws SQLException, ClassNotFoundException, IOException {
        List<Map<String, String>> result = new ArrayList<>();

        for (Map<String, Object> objectMap : resultSetToListOfObjectMaps(connection, resultSet)) {
            result.add(convertToStringMap(objectMap));
        }

        return result;
    }

    public void executeQuery(String sql) throws SQLException {
        executeQuery(sql, false);
    }

    public void executeQuery(String sql, Boolean runOnce) throws SQLException {
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {

            logger.debug(sql);

            if (runOnce) {
                if (resultSet.next()) {
                    use(resultSetToObjectMap(resultSet), 1);
                }
            } else {
                List<Map<String, Object>> resultList = resultSetToListOfObjectMaps(resultSet);
                int i = 1;
                for (Map<String, Object> rowData : resultList) {
                    use(rowData, i++);
                }
            }
        } catch (SQLException | ClassNotFoundException | IOException e) {
            logger.error(sql);
            throw new SQLException(e);
        }
    }

    // TODO : stream
    private static Map<String, String> convertToStringMap(Map<String, Object> objectMap) {
        Map<String, String> stringMap = new LinkedHashMap<>(objectMap.size());
        for (Map.Entry<String, Object> entry : objectMap.entrySet()) {
            stringMap.put(entry.getKey(), entry.getValue() == null ? null : entry.getValue().toString());
        }
        return stringMap;
    }

    /**
     * clobObject will be freed !!!
     *
     * @param clobObject
     * @return
     * @throws SQLException
     * @throws IOException
     */
    private static String clobToString(Clob clobObject) throws SQLException, IOException {
        if (clobObject == null) {
            return null;
        }

        InputStream in = clobObject.getAsciiStream();
        StringWriter w = new StringWriter();
        IOUtils.copy(in, w, Charset.defaultCharset());
        clobObject.free();
        return w.toString();
    }

    /**
     * blobObject will be freed !!!
     *
     * @param blobObject
     * @return array of bytes
     * @throws SQLException
     */
    private static byte[] blobToByteArray(Blob blobObject) throws SQLException {
        if (blobObject == null) {
            return null;
        }

        int blobLength = (int) blobObject.length();
        byte[] blobAsBytes = blobObject.getBytes(1, blobLength);
        blobObject.free();
        return blobAsBytes;
    }

}
