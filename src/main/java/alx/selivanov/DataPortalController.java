// DataPortalController.java

package alx.selivanov;

import alx.selivanov.jdbc.ConnectionPool;
import alx.selivanov.jdbc.DAOBase;
import alx.selivanov.jdbc.DAOOracle;
import alx.selivanov.jdbc.DAOPostgre;
import org.slf4j.Logger;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataPortalController {

    private final ConnectionPool sourceCP;
    private final ConnectionPool targetCP;
    private final String[] mainTablesNameRegExps; // первоочередной набор таблиц
    private final String[] mainExcludedTablesNameRegExps;
    private final String[] secondTablesNameRegExps; // набор оставшихся таблиц
    private final String[] secondExcludedTablesNameRegExps;
    private final List<String> preImportTransformers; // обработчики, запускаемые перед запуском переноса каждого из наборов таблиц
    private final List<String> postImportTransformers; // обработчики, запускаемые после переноса каждого из наборов таблиц
    private final Map<String, String[]> preTableTransformers;// обработчики, запускаемые перед запуском переноса опредедённой таблицы
    private final Map<String, String[]> dataTableTransformers; // обработчики, запускаемые после считывания данных определённой таблицы (каждой из частей данных)
    private final Map<String, String[]> postTableTransformers; // обработчики, запускаемые после переноса опредедённой таблицы
    private final int MAX_THREADS;
    private final int MAX_ROWS;
    private final Logger rootLogger;

    public DataPortalController(ConnectionPool sourceCP, ConnectionPool targetCP, String[] mainTablesNameRegExps, String[] mainExcludedTablesNameRegExps, String[] secondTablesNameRegExps, String[] secondExcludedTablesNameRegExps, List<String> preImportTransformers, List<String> postImportTransformers, Map<String, String[]> preTableTransformers, Map<String, String[]> dataTableTransformers, Map<String, String[]> postTableTransformers, int maxThreads, int maxRows, Logger rootLogger) {
        this.sourceCP = sourceCP;
        this.targetCP = targetCP;
        this.mainTablesNameRegExps = mainTablesNameRegExps;
        this.mainExcludedTablesNameRegExps = mainExcludedTablesNameRegExps;
        this.secondTablesNameRegExps = secondTablesNameRegExps;
        this.secondExcludedTablesNameRegExps = Stream.concat(Arrays.stream(secondExcludedTablesNameRegExps), Arrays.stream(mainTablesNameRegExps)).toArray(String[]::new);
        this.preImportTransformers = preImportTransformers;
        this.postImportTransformers = postImportTransformers;
        this.preTableTransformers = preTableTransformers;
        this.dataTableTransformers = dataTableTransformers;
        this.postTableTransformers = postTableTransformers;
        MAX_THREADS = maxThreads;
        MAX_ROWS = maxRows;
        this.rootLogger = rootLogger;

    }

    public void run() throws Exception {
        DAOBase sourceDAO = new DAOOracle(sourceCP);
        Set<String> mainTableNames = new LinkedHashSet<>(sourceDAO.getTableNames(mainTablesNameRegExps, mainExcludedTablesNameRegExps));

        try {
            sourceDAO.getConnection().close();
        } catch (SQLException e) {
            sourceDAO.getLogger().error(e.getMessage(), e);
        }
        sourceDAO = null;

        Object extras = null;

        for (String tranformer : preImportTransformers) {
            extras = TransformerRunner.runTransformer("preImportTransformer", tranformer, sourceCP, targetCP, mainTableNames, Collections.emptyList(), rootLogger, extras);
        }
        rootLogger.info("Starting migration of main tables: " + mainTableNames);

        runForTables(mainTableNames);

        for (String tranformer : postImportTransformers) {
            extras = TransformerRunner.runTransformer("postImportTransformer", tranformer, sourceCP, targetCP, mainTableNames, Collections.emptyList(), rootLogger, extras);
        }

        sourceDAO = new DAOOracle(sourceCP);
        Set<String> secondTableNames = new LinkedHashSet<>(sourceDAO.getTableNames(secondTablesNameRegExps, secondExcludedTablesNameRegExps));
        secondTableNames.removeAll(mainTableNames);

        try {
            sourceDAO.getConnection().close();
        } catch (SQLException e) {
            sourceDAO.getLogger().error(e.getMessage(), e);
        }
        sourceDAO = null;

        for (String tranformer : preImportTransformers) {
            extras = TransformerRunner.runTransformer("preImportTransformer", tranformer, sourceCP, targetCP, mainTableNames, Collections.emptyList(), rootLogger, extras);
        }
        rootLogger.info("Starting migration of second tables: " + secondTableNames);

        runForTables(secondTableNames);
        for (String tranformer : postImportTransformers) {
            extras = TransformerRunner.runTransformer("postImportTransformer", tranformer, sourceCP, targetCP, mainTableNames, Collections.emptyList(), rootLogger, extras);
        }

        rootLogger.info("All tasks done");
    }

    private List<String> getTransformers(Map<String, String[]> transformersMap, String table) {
        List<String> result = new ArrayList<>();

        for (Map.Entry<String, String[]> entry : transformersMap.entrySet()) {
            String tableNameRegExp = entry.getKey();

            Pattern pattern = Pattern.compile(tableNameRegExp);
            if (pattern.matcher(table).find()) {
                result.addAll(Arrays.asList(entry.getValue()));
            }
        }

        return result;
    }

    private void runForTables(Set<String> tableNames) throws InterruptedException {
        List<Callable<String>> taskList = tableNames.stream().map(tableName -> new DataPortal(new DAOOracle(sourceCP), new DAOPostgre(targetCP), tableName, MAX_ROWS, getTransformers(preTableTransformers, tableName), getTransformers(dataTableTransformers, tableName), getTransformers(postTableTransformers, tableName), rootLogger)).collect(Collectors.toList());
        ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREADS);

        try {
            executorService.invokeAll(taskList).stream()
                    .map(f -> {
                        try {
                            return f.get();
                        } catch (Exception e) {
                            rootLogger.error(e.getMessage(), e);
                        }
                        return null;
                    })
                    .forEach(r -> rootLogger.info("Work result = " + r));
        } catch (InterruptedException e) {
            rootLogger.error(e.getMessage(), e);
            executorService.shutdownNow();
            throw e;
        }

        executorService.shutdown();
    }

}
