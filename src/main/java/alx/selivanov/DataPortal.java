// DataPortal.java

package alx.selivanov;

import alx.selivanov.jdbc.DAOBase;
import alx.selivanov.utils.Utils;
import org.slf4j.Logger;

import javax.xml.datatype.DatatypeConfigurationException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.concurrent.Callable;

public class DataPortal implements Callable<String> {

    private final DAOBase sourceDAO;
    private final DAOBase targetDAO;
    private final String tableName;
    private final Long rowCount;
    private final List<String> preTableTransformers;
    private final List<String> dataTableTransformers;
    private final  List<String> postTableTransformers;
    private final Logger rootLogger;
    private final int rowOffset;

    public DataPortal(DAOBase sourceDAO, DAOBase targetDAO, String tableName, int rowOffset, List<String> preTableTransformers, List<String> dataTableTransformers, List<String> postTableTransformers, Logger rootLogger) {
        this.preTableTransformers = preTableTransformers;
        this.dataTableTransformers = dataTableTransformers;
        this.postTableTransformers = postTableTransformers;
        this.rootLogger = rootLogger;
        sourceDAO.getLogger().debug("Open Data Portal for " + tableName);
        this.sourceDAO = sourceDAO;
        this.targetDAO = targetDAO;
        this.tableName = tableName;
        this.rowOffset = rowOffset;
        this.rowCount = sourceDAO.getTableRowCount(tableName);
        sourceDAO.getLogger().debug("Data Portal opened for " + tableName);

    }

    private List<Map<String, Object>> readData(long beginRowNum, long endRowNum) {

        String sql = sourceDAO.getSelectAllFromRowToRowQuery(tableName, beginRowNum, endRowNum);

        return sourceDAO.readAsListOfMaps("\n" + sql);
    }

    private int[] writeData(List<Map<String, Object>> data) throws Exception {
        int[] updateCount = new int[]{};

        if (data == null || data.size() < 1) {
            return updateCount;
        }

        Connection connection = targetDAO.getConnection();

        String columnName = "";
        try {
            connection.setAutoCommit(false);

            String preparedSqlString = "INSERT INTO " + tableName + "(";
            StringJoiner columnNamesJoiner = new StringJoiner(",");
            StringJoiner valuesJoiner = new StringJoiner(",");

            for (Map.Entry<String, Object> entry : data.get(0).entrySet()) {
                columnName = entry.getKey();
                if (!columnName.equals(columnName.toLowerCase()) && !columnName.equals(columnName.toUpperCase())) {
                    columnName = "\"" + columnName + "\"";
                }
                columnNamesJoiner.add(columnName);
                valuesJoiner.add("?");
            }

            preparedSqlString += columnNamesJoiner.toString() + ") VALUES (" + valuesJoiner.toString() + ")";

            PreparedStatement preparedStatement = connection.prepareStatement(preparedSqlString);
            Map<String, Class> columnClasses = targetDAO.getTableColumnClasses(tableName);

            for (Map<String, Object> rowData : data) {
                int i = 1;

                for (Map.Entry<String, Object> entry : rowData.entrySet()) {
                    columnName = entry.getKey();
                    Object value = Utils.changeType(entry.getValue(), columnClasses.get(entry.getKey()));
                    preparedStatement.setObject(i++, value);
                }

                preparedStatement.addBatch();
            }

            updateCount = preparedStatement.executeBatch();

            connection.commit();

        } catch (SQLException e) {
            targetDAO.getLogger().error(e.getMessage() + "\ncolumnName = " + columnName, e);
            try {
                connection.rollback();
            } catch (SQLException e1) {
                targetDAO.getLogger().error(e.getMessage(), e1);
            }
            throw e;
        } catch (ParseException | DatatypeConfigurationException e) {
            targetDAO.getLogger().error(e.getMessage(), e);
            throw new Exception(e);
        } finally {
            try {
                connection.setAutoCommit(true);
                connection.close();
            } catch (SQLException e) {
                targetDAO.getLogger().error(e.getMessage(), e);
            }
        }

        return updateCount;
    }

    @Override
    public String call() throws Exception {
        long startTime = System.currentTimeMillis();
        long currentTime;

        Object extras = null;
        for (String tranformer : preTableTransformers) {
            extras = TransformerRunner.runTransformer("preTableTransformers", tranformer, sourceDAO.getConnectionPool(), targetDAO.getConnectionPool(), Collections.singleton(tableName), Collections.emptyList(), rootLogger, extras);
        }

        for (long beginRowNum = 0; beginRowNum < rowCount; beginRowNum += rowOffset) {
            List<Map<String, Object>> data = readData(beginRowNum + 1, beginRowNum + rowOffset);

            for (String tranformer : dataTableTransformers) {
                extras = TransformerRunner.runTransformer("dataTableTransformers", tranformer, sourceDAO.getConnectionPool(), targetDAO.getConnectionPool(), Collections.singleton(tableName), data, rootLogger, extras);
            }

            writeData(data);
            currentTime = System.currentTimeMillis();

            targetDAO.getLogger().info(tableName + " " + ((beginRowNum + rowOffset) >= rowCount ? rowCount : (beginRowNum + rowOffset)) + " rows migrated, time elapsed (ms): " + (currentTime - startTime));
        }

        if (sourceDAO.getConnection() != null) {
            sourceDAO.getConnection().close();
        }
        if (targetDAO.getConnection() != null) {
            targetDAO.getConnection().close();
        }

        for (String tranformer : postTableTransformers) {
            extras = TransformerRunner.runTransformer("postTableTransformers", tranformer, sourceDAO.getConnectionPool(), targetDAO.getConnectionPool(), Collections.singleton(tableName), Collections.emptyList(), rootLogger, extras);
        }

        currentTime = System.currentTimeMillis();

        return tableName + " (" + rowCount + " rows) in " + (currentTime - startTime) + " ms DONE";
    }

    @Override
    protected void finalize() throws Throwable {
        if (sourceDAO.getConnection() != null && !sourceDAO.getConnection().isClosed()) {
            sourceDAO.getConnection().close();
        }
        if (targetDAO.getConnection() != null && !targetDAO.getConnection().isClosed()) {
            targetDAO.getConnection().close();
        }
    }
}
