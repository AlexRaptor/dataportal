package alx.selivanov.utils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JSONUtilsTest {

    private final String resourcesDirectory = new File("src/test/resources").getAbsolutePath();
    private final Map<String, Object> expectedMap = new HashMap<>(2);

    @Before
    public void setUpMap() {

        Map<String, String> map = new HashMap<>(2);
        map.put("key1", "value1");
        map.put("key2", "value2");
        expectedMap.put("testObj", map);
        expectedMap.put("testArray", Arrays.asList("arrayValue1", "arrayValue2"));
    }

    @After
    public void tearDownExpectedMap() {

        expectedMap.clear();

    }

    @Test
    public void readJSONfromFileAsMap() {

        Map<String, Object> testMap = JSONUtils.readJSONfromFileAsMap(resourcesDirectory + "/testJSON.json");

        assertTrue(testMap != null);

        for (Map.Entry<String, Object> entry : testMap.entrySet()) {

            Object actual = entry.getValue();
            Object expected = expectedMap.get(entry.getKey());

            assertEquals(expected, actual);

        }
    }
}